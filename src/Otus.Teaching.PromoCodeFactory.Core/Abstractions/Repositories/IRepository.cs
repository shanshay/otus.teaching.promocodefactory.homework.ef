﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRepository<T>  where T: BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync();        
        Task<T> GetByIdAsync(Guid id);
        Task<IEnumerable<T>> GetByIdsAsync(List<Guid> ids);
        Task<T> GetOneWhere(Expression<Func<T, bool>> flag);
        Task<IEnumerable<T>> GetWhere(Expression<Func<T, bool>> flag);
        Task CreateAsync(T entity);
        Task UpdateAsync(T entity = null);
        Task DeleteAsync(T entity);      
    }
}